'use strict';

module.exports = {
  paths: {
    src: {
      html: 'src/index.html'
    },
    dest: {
      html: 'build'
    }
  }
};
