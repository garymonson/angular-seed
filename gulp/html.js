'use strict';

module.exports = function(gulp, src, dest) {
  return function() {
    gulp.src(src)
      .pipe(gulp.dest(dest))
      ;
  };
};
