'use strict';

var gulp = require('gulp');

var config = require('./gulp/config.js');

gulp.task('html', require('./gulp/html.js')(gulp, config.paths.src.html, config.paths.dest.html));

gulp.task('build', ['html']);
